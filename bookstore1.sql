/*
 Navicat Premium Data Transfer

 Source Server         : store
 Source Server Type    : MySQL
 Source Server Version : 100427
 Source Host           : localhost:3306
 Source Schema         : bookstore1

 Target Server Type    : MySQL
 Target Server Version : 100427
 File Encoding         : 65001

 Date: 17/04/2023 09:55:37
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin`  (
  `Aid` int NOT NULL AUTO_INCREMENT,
  `AdminUser` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `AdminPwd` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '管理员密码',
  `RealName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '管理员真实姓名',
  `phone` int NOT NULL COMMENT '管理员电话',
  `AdminArea` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '管理员管理区域',
  PRIMARY KEY (`Aid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES (1, 'root', 'root123', '管理员', 888888, '主管理员账号');
INSERT INTO `admin` VALUES (2, 'wuhao', 'w147258', '李三', 2147483647, 'A区');
INSERT INTO `admin` VALUES (3, 'liuliu666', 'liu888', '刘六', 2147483647, 'B区');

-- ----------------------------
-- Table structure for applybook
-- ----------------------------
DROP TABLE IF EXISTS `applybook`;
CREATE TABLE `applybook`  (
  `ABid` int NOT NULL AUTO_INCREMENT COMMENT '预约申请表id',
  `BookName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图书名称',
  `ApplyName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '预约账号',
  `UserAddress` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户地址',
  `ApplyTime` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '预约时间',
  `State` tinyint(1) NOT NULL DEFAULT 2 COMMENT '预约状态 0为通过 1为拒绝',
  PRIMARY KEY (`ABid`, `State`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of applybook
-- ----------------------------
INSERT INTO `applybook` VALUES (1, '《平凡的世界》', '系统管理员', 'A区三栋502', '2023-04-26', 0);
INSERT INTO `applybook` VALUES (2, '《狼王》', '丽丽', 'A区三栋555', '2023-04-26', 0);
INSERT INTO `applybook` VALUES (3, '《靠山》', '张三', 'A区五栋308', '2023-04-26', 1);
INSERT INTO `applybook` VALUES (4, '《钢铁是怎样炼成的》', '系统管理员', 'A区三栋502', '2023-04-26', 1);
INSERT INTO `applybook` VALUES (5, '《论中国共产党历史》', '系统管理员', 'A区三栋502', '2023-04-26', 0);
INSERT INTO `applybook` VALUES (6, '《三只小猪》', '系统管理员', 'A区三栋502', '2023-04-26', 0);
INSERT INTO `applybook` VALUES (7, '《巴黎圣母院》', '系统管理员', 'A区三栋502', '2023-04-26', 1);
INSERT INTO `applybook` VALUES (8, '《乱世佳人》', '系统管理员', 'A区三栋502', '2023-04-26', 0);
INSERT INTO `applybook` VALUES (9, '《鲁滨逊漂流记》', '系统管理员', 'A区三栋502', '2023-04-26', 0);
INSERT INTO `applybook` VALUES (10, '《论中国共产党历史》', 'wuhao', '呼伦贝尔', '2023-04-26', 0);
INSERT INTO `applybook` VALUES (11, '《狼王》', 'wuhao888', '呼市', '2023-04-26', 0);
INSERT INTO `applybook` VALUES (12, '《论中国共产党历史》', 'wuhao888', '呼市', '2023-04-26', 0);
INSERT INTO `applybook` VALUES (13, '《数字解读中国：中国的发展坐标与发展成就》', 'wuhao888', '呼市', '2023-04-26', 0);
INSERT INTO `applybook` VALUES (14, '《格列佛游记》', 'wuhao888', '呼市', '2023-04-26', 0);
INSERT INTO `applybook` VALUES (15, '《大医马海德》', 'wuhao888', '呼市', '2023-04-26', 2);
INSERT INTO `applybook` VALUES (16, '《格列佛游记》', 'wuhao888', '呼市', '2023-04-26', 2);

-- ----------------------------
-- Table structure for books
-- ----------------------------
DROP TABLE IF EXISTS `books`;
CREATE TABLE `books`  (
  `Bid` int NOT NULL AUTO_INCREMENT COMMENT '图书序列号',
  `BookName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图书名称',
  `BookAuthor` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图书作者',
  `BookLocation` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图书存放位置',
  `BooksSum` int NOT NULL COMMENT '图书库存数量',
  `ApplySum` int(1) UNSIGNED ZEROFILL NULL DEFAULT NULL COMMENT '图书已经预约库存数量',
  `State` tinyint(1) UNSIGNED ZEROFILL NOT NULL COMMENT '图书状态  0为上架 1为下架',
  PRIMARY KEY (`Bid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of books
-- ----------------------------
INSERT INTO `books` VALUES (3, '《论中国共产党历史》', '中央文献出版社', '01-1A-103', 100, 0, 1);
INSERT INTO `books` VALUES (5, '《靠山》', '铁流', '01-1A-105', 100, 0, 0);
INSERT INTO `books` VALUES (6, '《大医马海德》', '陈郭德', '01-1A-106', 100, 0, 1);
INSERT INTO `books` VALUES (7, '《数字解读中国：中国的发展坐标与发展成就》', '贺耀敏、甄峰', '01-1A-107', 100, 0, 0);
INSERT INTO `books` VALUES (9, '《钢铁是怎样炼成的》', '奥斯特洛夫斯基', '01-1A-109', 100, 0, 1);
INSERT INTO `books` VALUES (10, '《巴黎圣母院》', '雨果', '01-2A-201', 100, 0, 1);
INSERT INTO `books` VALUES (11, '《悲惨世界》', '雨果', '01-2A-202', 100, 60, 0);
INSERT INTO `books` VALUES (12, '《鲁滨逊漂流记》', '丹尼尔笛福', '01-2A-203', 100, 0, 0);
INSERT INTO `books` VALUES (13, '《乱世佳人》', '玛格丽特米切尔', '01-2A-204', 50, 0, 0);
INSERT INTO `books` VALUES (14, '《格列佛游记》', '乔纳森斯威夫特', '01-2A-205', 80, 0, 0);
INSERT INTO `books` VALUES (15, '《三只小猪》', '刘禅', '01-2A-206', 80, 0, 0);
INSERT INTO `books` VALUES (16, '《狼王》', '张三', '01-2A-207', 50, 0, 0);
INSERT INTO `books` VALUES (17, '葫芦娃', '张三', '01-2A-208', 20, 0, 1);
INSERT INTO `books` VALUES (18, '《青春》', '六子', '宝塔', 10, 0, 0);

-- ----------------------------
-- Table structure for borrowbooks
-- ----------------------------
DROP TABLE IF EXISTS `borrowbooks`;
CREATE TABLE `borrowbooks`  (
  `Bid` int NOT NULL AUTO_INCREMENT,
  `BorrowBook` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '借用图书',
  `BorrowUser` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '借用用户',
  `BorrowPhone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '借用用户手机号',
  `BorrowStartTime` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '借用开始时间',
  `BorrowOverTime` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '借用结束时间',
  `State` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '借用状态 0借用中 1已归还',
  PRIMARY KEY (`Bid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of borrowbooks
-- ----------------------------
INSERT INTO `borrowbooks` VALUES (21, '《论中国共产党历史》', 'wuhao1', '15848353562', '2023-03-29', '2023-04-25', '1');
INSERT INTO `borrowbooks` VALUES (22, '《青春》', 'wuhao1', '15848353562', '2023-04-04', '2023-04-25', '1');
INSERT INTO `borrowbooks` VALUES (23, '《乱世佳人》', 'wuhao', '17647374243', '2023-03-28', '2023-04-24', '1');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `uid` int NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `uname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户账号',
  `upwd` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户密码',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户手机号',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'wuhao', 'w147258', '17647374243');
INSERT INTO `users` VALUES (2, 'wuhao1', 'w147258', '15848353562');
INSERT INTO `users` VALUES (3, 'wuhao888', 'w147258', '17647374243');

SET FOREIGN_KEY_CHECKS = 1;
