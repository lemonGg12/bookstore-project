# 书店项目

#### 介绍

- 简单的一个书店项目，vue动态试手项目，目前已经部署到服务器   http://121.40.190.88  上，可供大家查看、功能逐渐开发中。
- 接口文档地址： https://console-docs.apipost.cn/preview/c834cccd2b0f121a/e84388d639933e34
- 默认管理员账号 root 密码sysadmin
- 默认用户账号 wuhao  密码 w147258 
- 系统管理员账号只可以在后台添加、用户账号可以自行注册。


#### 软件架构
软件架构说明
    前端：vue+vueRouter+vuex
    后端：node.js+express+mysql
#### 安装教程

1.  服务器根据packpage.JSON使用  `npm install` 命令安装模块包
2.  使用 `npm insatll axios`   安装axios组件
3.  使用 `npm i element-ui -S` 安装element-ui组件

#### 使用说明

1.  首先启动数据库，需要在node.js中配制你自己的数据库账号密码 默认为用户名为root 密码为空
2.  其次启动服务器 在服务器目录下 输入 `node app.js`命令 启动服务器 出现服务器跑起来了即为启动成功
3.  最后在vue中输入`npm run serve` 出现本地地址即可通过本地地址访问网页

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
1. 这里是列表文本