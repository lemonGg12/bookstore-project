import Vue from 'vue'
import VueRouter from 'vue-router'
import UserLogin from '../views/User/UserLogin.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'userlogin',
    component: UserLogin,
    meta:{
      title:"登录"
    },
  },
  {
    path: '/register',
    name: 'register',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/User/UserRegister.vue'),
    meta:{
      title:"注册"
    },
  },
  {
    path: '/index',
    name: 'index',
    component: () => import(/* webpackChunkName: "about" */ '../views/User/BookStores.vue'),
    meta:{
      title:"青花瓷书店"
    },
    children:[   // 添加子路由
    {
      // 添加图书查询子路由  页面
      path: 'BookQuery',  // 注意点：路径前面不要带/，否则无法跳转，在这里吃过亏
      components: {   // 注意参数名带s
        table: () => import('@/views/User/BookQuery')  // 这里的table跟首页的router-view标签的name一致，才会在首页的路由视图进行跳转，看3.2
      } ,
      meta:{
        title:"查询图书"
      },
    },
    {
      // 添加图书预约登记子路由  页面
      path: 'UserApply',  // 注意点：路径前面不要带/，否则无法跳转，在这里吃过亏
      components: {   // 注意参数名带s
        table: () => import('@/views/User/UserApply')  // 这里的table跟首页的router-view标签的name一致，才会在首页的路由视图进行跳转，看3.2
      } ,
      meta:{
        title:"用户预约申请"
      },
    },
    {
      // 添加图书预约记录子路由  页面
      path: 'ApplyRecord',  // 注意点：路径前面不要带/，否则无法跳转，在这里吃过亏
      components: {   // 注意参数名带s
        table: () => import('@/views/User/ApplyRecord')  // 这里的table跟首页的router-view标签的name一致，才会在首页的路由视图进行跳转，看3.2
      } ,
      meta:{
        title:"用户预约申请"
      },
    },
    {
      // 添加借阅记录子路由  页面
      path: 'BorrowBooks',  // 注意点：路径前面不要带/，否则无法跳转，在这里吃过亏
      components: {   // 注意参数名带s
        table: () => import('@/views/User/BorrowBooks')  // 这里的table跟首页的router-view标签的name一致，才会在首页的路由视图进行跳转，看3.2
      } ,
      meta:{
        title:"借阅记录"
      },
    },
  ],
  },



  {
    path: '/AdminHome',
    name: 'AdminHome',
    component: () => import(/* webpackChunkName: "about" */ '../views/Admin/AdminHome.vue'),
    children:[   // 添加子路由
    {
      // 添加图书子路由  页面
      path: 'AddBooks',  // 注意点：路径前面不要带/，否则无法跳转，在这里吃过亏
      components: {   // 注意参数名带s
        table: () => import('@/views/Admin/AddBooks')  // 这里的table跟首页的router-view标签的name一致，才会在首页的路由视图进行跳转，看3.2
      } ,
      meta:{
        title:"添加书籍"
      },
    },
    {
      // 添加图书查询设置删除子路由  页面
      path: 'BookSet',  // 注意点：路径前面不要带/，否则无法跳转，在这里吃过亏
      components: {   // 注意参数名带s
        table: () => import('@/views/Admin/BookSet')  // 这里的table跟首页的router-view标签的name一致，才会在首页的路由视图进行跳转，看3.2
      } ,
      meta:{
        title:"书籍查询设置"
      },
    },
    {
      // 添加管理员管理子路由  页面
      path: 'AdminQuery',  // 注意点：路径前面不要带/，否则无法跳转，在这里吃过亏
      components: {   // 注意参数名带s
        table: () => import('@/views/Admin/AdminQuery')  // 这里的table跟首页的router-view标签的name一致，才会在首页的路由视图进行跳转，看3.2
      },
      meta:{
        title:"管理员查询"
      },
    },
    {
      // 添加管理员管理子路由  页面
      path: 'AdminAdd',  // 注意点：路径前面不要带/，否则无法跳转，在这里吃过亏
      components: {   // 注意参数名带s
        table: () => import('@/views/Admin/AdminAdd')  // 这里的table跟首页的router-view标签的name一致，才会在首页的路由视图进行跳转，看3.2
      },
      meta:{
        title:"管理员新增"
      },
    },
    {
      // 添加预约记录子路由  页面
      path: 'ApplyBooks',  // 注意点：路径前面不要带/，否则无法跳转，在这里吃过亏
      components: {   // 注意参数名带s
        table: () => import('@/views/Admin/ApplyBooks')  // 这里的table跟首页的router-view标签的name一致，才会在首页的路由视图进行跳转，看3.2
      },
      meta:{
        title:"预约记录"
      },
    },
    {
      // 添加系统用户借阅图书子路由  页面
      path: 'UserBorrow',  // 注意点：路径前面不要带/，否则无法跳转，在这里吃过亏
      components: {   // 注意参数名带s
        table: () => import('@/views/Admin/UserBorrow')  // 这里的table跟首页的router-view标签的name一致，才会在首页的路由视图进行跳转，看3.2
      },
      meta:{
        title:"系统用户借阅"
      },
    },
    {
      // 添加非管理员借用图书子路由  页面
      path: 'NoUserBorrow',  // 注意点：路径前面不要带/，否则无法跳转，在这里吃过亏
      components: {   // 注意参数名带s
        table: () => import('@/views/Admin/NoUserBorrow')  // 这里的table跟首页的router-view标签的name一致，才会在首页的路由视图进行跳转，看3.2
      },
      meta:{
        title:"非系统用户借阅"
      },
    },
    {
      // 管理员查询借阅记录子路由  页面
      path: 'BorrowQuery',  // 注意点：路径前面不要带/，否则无法跳转，在这里吃过亏
      components: {   // 注意参数名带s
        table: () => import('@/views/Admin/BorrowQuery')  // 这里的table跟首页的router-view标签的name一致，才会在首页的路由视图进行跳转，看3.2
      },
      meta:{
        title:"系统用户借阅"
      },
    },
  ],
    meta:{
      title:"管理员后台"
    },
  },
  {
    path: '/AdminLogin',
    name: 'AdminLogin',
    component: () =>import(/* webpackChunkName: "about" */ '../views/Admin/AdminLogin.vue'),
    meta:{
      title:"管理员登录"
    },
  },
  // {
  //   path: '/Admin/Addbooks',
  //   component: () =>import(/* webpackChunkName: "about" */ '../views/Admin/AddBooks.vue'),
  //   meta:{
  //     title:"添加书籍"
  //   },
  // }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
router.beforeEach((to, from, next) => {
  /* 路由发生变化修改页面title */
  if (to.meta.title) {
    document.title = to.meta.title
  }
  next()
})


export default router
